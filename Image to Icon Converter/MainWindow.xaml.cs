﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace Image_to_Icon_Converter
{
    static class BitConverter
    {
        public static BitmapImage ToBitmapImage(this Bitmap bitmap)
        {
            using (var memory = new System.IO.MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Png);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        // Variables
        string filename;
        bool canSave;

        public MainWindow()
        {
            InitializeComponent();
            canSave = false;
        }  

        private void btn_selectDialog_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".png";
            dlg.Filter = "All Files (*.*)|*.*|" +
                "Bitmap Image File (*.bmp)|*.bmp|" +
                "Graphical Interchange Format File (*.gif)|*.gif|" +
                "JPEG Image (*.jpg)|*.jpg|" +
                "Portable Network Graphic (*.png)|*.png|" +
                "Tagged Image File (*.tif)|*.tif";
            
            /* (Bitmap support following formats, so it should work with the program.
            Howerver, not included due to not tested not that common formats. add and test if you want :) )
            
            "Exchangeable image file format (*.exif)|*.exif|" +
            "Windows Metafile (*.wmf)|*.wmf|" +
            "Enhanced Metafile (*.emf)|*.emf";
            */


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and load the image.
            if (result == true)
            {
                // Open document 
                filename = dlg.FileName;

                // Load and setup new image.
                using (var bmpTemp = new Bitmap(filename))
                {
                    BitmapImage img = new BitmapImage();
                    img = BitConverter.ToBitmapImage(bmpTemp);
                    img_PictureToConvert.Source = img;
                }

                    canSave = true;
            }
            
        }

        private void btn_saveIcon_Click(object sender, RoutedEventArgs e)
        {
            if (canSave == false)
                return;

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = System.IO.Path.GetFileNameWithoutExtension(filename); // Default file name
            dlg.DefaultExt = ".ico"; // Default file extension
            dlg.Filter = "Icon (.ico)|*.ico"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string saveLocation = dlg.FileName;
                // Get resolution for icon
                int icoRes = getResolution();
                if (icoRes == 0)
                {
                    MessageBox.Show("Icon Ressolution Error!/nMake sure you have a valid icon resolution checked.");
                    return;
                }


                if (img_PictureToConvert.OpacityMask != null)
                {
                    //Save tmp picture that has outline.
                    SaveToPng(grid_PrintMe, saveLocation + "-OM.tmp");
                    //Convert tmp picture to ico file.
                    PngIconConverter.Convert(saveLocation + "-OM.tmp", saveLocation, icoRes);
                    //Delete tmp file.
                    System.IO.File.Delete(saveLocation + "-OM.tmp");
                }
                else
                    PngIconConverter.Convert(filename, saveLocation, icoRes);
 
            }
        }
        // Returns selected resolution
        private int getResolution()
        {
            if (rb_16x.IsChecked.Value)
                return 16;
            else if (rb_32x.IsChecked.Value)
                return 32;
            else if (rb_64x.IsChecked.Value)
                return 64;
            else if (rb_128x.IsChecked.Value)
                return 128;
            else if (rb_256x.IsChecked.Value)
                return 256;

            return 0;
        }

        private void cb_folderMask_Click(object sender, RoutedEventArgs e)
        {
            if (cb_folderMask.IsChecked.Value)
            {
                using (var bmpTemp = new Bitmap(Image_to_Icon_Converter.Properties.Resources.folderFilter))
                {     
                    BitmapImage img = new BitmapImage();
                    img = BitConverter.ToBitmapImage(bmpTemp);

                    ImageBrush maskBrush = new ImageBrush();
                    maskBrush.ImageSource = img;

                    img_PictureToConvert.OpacityMask = maskBrush;
                }

            }
            else
                img_PictureToConvert.OpacityMask = null;
        }




        void SaveToPng(FrameworkElement visual, string fileName)
        {
            var encoder = new PngBitmapEncoder();
            SaveUsingEncoder(visual, fileName, encoder);
        }

        void SaveUsingEncoder(FrameworkElement visual, string fileName, BitmapEncoder encoder)
        {
            RenderTargetBitmap bitmap = new RenderTargetBitmap((int)visual.ActualWidth, (int)visual.ActualHeight, 96, 96, PixelFormats.Pbgra32);
            bitmap.Render(visual);
            BitmapFrame frame = BitmapFrame.Create(bitmap);
            encoder.Frames.Add(frame);

            using (var stream = System.IO.File.Create(fileName))
            {
                encoder.Save(stream);
            }
        }

        private void cb_folderOutline_Click(object sender, RoutedEventArgs e)
        {
            if (cb_folderOutline.IsChecked.Value)
                img_FolderOuntline.Visibility = Visibility.Visible;
            else
                img_FolderOuntline.Visibility = Visibility.Collapsed;
        }
    }
}
